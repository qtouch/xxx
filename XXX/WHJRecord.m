//
//  WHJRecord.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "WHJRecord.h"
#import "WHJDocument.h"

@implementation WHJRecord{
    NSMutableArray* _documents;
}
@synthesize documents = _documents;

-(instancetype)init{
    if (self = [super init]) {
        _documents = [NSMutableArray array];
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [self init]) {
        _title = dictionary[@"title"];
        _date = dictionary[@"date"];
        NSArray* docs = dictionary[@"documents"];
        for (NSDictionary* dict in docs) {
            WHJDocument* d = [[WHJDocument alloc]initWithDictionary:dict];
            d.record = self;
            [self addDocument:d];
        }
    }
    return self;
}

-(void)addDocument:(WHJDocument *)doc{
    [_documents addObject:doc];
}

@end
