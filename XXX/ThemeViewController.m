//
//  ThemeViewController.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "ThemeViewController.h"
#import "DataController.h"
#import "RecodViewController.h"

@interface ThemeViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak,nonatomic) DataController* dc;

@end

@implementation ThemeViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.navigationItem.title = [self.dc titleThemeOfIndex:_themeIndex];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dc numRecordOfThemeIndex:_themeIndex];
}

-(DataController *)dc{
    if (!_dc) {
        _dc = [DataController sharedDataController];
    }
    return _dc;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* t = [tableView dequeueReusableCellWithIdentifier:kRecordCellID forIndexPath:indexPath];
    [self configureCell:t withIndexPath:indexPath];
    return t;
}

-(void)configureCell:(UITableViewCell*)tvc withIndexPath:(NSIndexPath*)ip{
    NSDictionary* d = [self.dc dictionaryOfRecordIndex:ip.item themeIndex:_themeIndex];
    tvc.textLabel.text = d[kRecordTitle];
    tvc.detailTextLabel.text = d[kRecordDate];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:kRecordSegue]) {
        RecodViewController* rvc = segue.destinationViewController;
        rvc.themeIndex = _themeIndex;
        rvc.recordIndex = [[self.tableView indexPathForCell:sender]item];
    }
}

@end
