//
//  WHJRecord.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "WHJDataObject.h"
@class WHJDocument,WHJTheme;

@interface WHJRecord : WHJDataObject

@property (nonatomic) NSString* title;
@property (nonatomic) NSString* date;
@property (nonatomic) NSArray* documents;

@property (nonatomic,weak) WHJTheme* theme;

-(void)addDocument:(WHJDocument *)doc;

@end
