//
//  WHJDataObject.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "WHJDataObject.h"

@implementation WHJDataObject

-(instancetype)initWithURL:(NSURL *)url{
    if (self =[self init]) {
        self.url = url;
    }
    return self;
}

-(instancetype)initWithFileName:(NSString *)name bundle:(NSString *)bdName{
    if (self = [self init]) {
        self.fileName = name;
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [self init]) {
        [self setValuesForKeysWithDictionary:dictionary];
    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        _url = nil;
        _fileName = nil;
        _bundleName = nil;
        _loadStatus = WHJDataObjectLoadStatusCantLoad;
    }
    return self;
}

-(void)setFileName:(NSString *)fileName{
    _fileName = fileName;
    if (_fileName) {
        _loadStatus = _loadStatus|WHJDataObjectLoadStatusCached;
    }else{
        _loadStatus = _loadStatus&(!WHJDataObjectLoadStatusCached);
    }
}

-(void)setUrl:(NSURL *)url{
    _url = url;
    if (_url) {
        _loadStatus = _loadStatus|WHJDataObjectLoadStatusOnline;
    }else{
        _loadStatus = _loadStatus&(!WHJDataObjectLoadStatusOnline);
    }
}
-(BOOL)load{
    return NO;
}

-(BOOL)save{
    return NO;
}

-(BOOL)unload{
    return NO;
}
@end
