//
//  WHJThemeCollectionViewCell.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WHJThemeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@end
