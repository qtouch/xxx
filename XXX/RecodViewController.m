//
//  RecodViewController.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "RecodViewController.h"
#import "DataController.h"

@interface RecodViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak,nonatomic) DataController* dc;


@end

@implementation RecodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = [self.dc titleOfRecordIndex:_recordIndex themeIndex:_themeIndex];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(DataController *)dc{
    if (!_dc) {
        _dc = [DataController sharedDataController];
    }
    return _dc;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dc numReplyOfRecordIndex:_recordIndex themeIndex:_themeIndex];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* tvc = [tableView dequeueReusableCellWithIdentifier:kSimpleCell forIndexPath:indexPath];
    [self configureCell:tvc withIndexPath:indexPath];
    return tvc;
}


-(void)configureCell:(UITableViewCell*)tvc withIndexPath:(NSIndexPath*)ip{
    NSDictionary* d = [self.dc dictionaryOfDocumentOfIndex:ip.item recordIndex:_recordIndex themeIndex:_themeIndex];
    tvc.textLabel.text = d[kDocumentDiscuss];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
