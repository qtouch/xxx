//
//  ThemeViewController.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString* kRecordCellID = @"record";
static NSString* kRecordSegue = @"goRecordDetail";

@interface ThemeViewController : UITableViewController

@property (nonatomic) NSUInteger themeIndex;

@end



