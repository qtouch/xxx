//
//  DataController.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "DataController.h"
#import "DataModel.h"


@implementation DataController{
    NSArray* _themes;
}

+(DataController*)sharedDataController{
    static DataController* sd;
    if (!sd) {
        sd = [[DataController alloc]init];
        if (!sd) {
            NSLog(@"123123");
            abort();
        }
    }
    return sd;
}

-(instancetype)init{
    if (self = [super init]) {
        [self loadThemes];
    }
    return self;
}

-(void)loadThemes{
    NSArray* ts = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Demo" ofType:@"plist"]];
    if (!ts) {
        NSLog(@"#######################Load Demo To Array Failed!!!#####################");
        abort();
    }
    NSMutableArray* mt = [NSMutableArray array];
    for (NSDictionary* d  in ts) {
        WHJTheme* t = [[WHJTheme alloc]initWithDictionary:d];
        [mt  addObject:t];
    }
    _themes = mt;
}

#pragma mark - Datasource

#pragma mark Theme
-(NSUInteger)numTheme{
    return _themes.count;
}

-(NSString *)titleThemeOfIndex:(NSUInteger)ti{
    return [(WHJTheme*)_themes[ti] title];
}

#pragma mark Record
-(NSUInteger)numRecordOfThemeIndex:(NSUInteger)ti{
    return [_themes[ti] records].count;
}

-(NSDictionary *)dictionaryOfRecordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti{
    WHJRecord* r = [_themes[ti] records][ri];
    return @{kRecordTitle:r.title,kRecordDate:r.date};
}

-(NSString *)titleOfRecordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti{
     return [[_themes[ti] records][ri] title];
}

#pragma mark Document
-(NSUInteger)numReplyOfRecordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti{
    return [[_themes[ti] records][ri] documents].count;
}

-(NSDictionary *)dictionaryOfDocumentOfIndex:(NSUInteger)di recordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti{
    WHJDocument* d = [[_themes[ti] records][ri] documents][di];
    return @{kDocumentDiscuss:d.discuss};
}

#pragma mark - LoadData
-(BOOL)loadDataObject:(WHJDataObject*)d{
    if (!d.loadStatus) {
        [self errorWithCode:1 userinfo:@{NSLocalizedFailureReasonErrorKey:@"Can't load object",@"object":d}];
        return NO;
    }
    if ((d.loadStatus&WHJDataObjectLoadStatusLoaded) == 0) {
        if (![d load]) {
            [self errorWithCode:2 userinfo:@{NSLocalizedFailureReasonErrorKey:@"Load Object Failed",@"object":d}];
            return NO;
        };
    }
    return YES;
}

-(void)errorWithCode:(NSUInteger)code userinfo:(NSDictionary*)dict{
    NSError* error = [NSError errorWithDomain:kDataControllerErrorKey code:code userInfo:dict];
    [[NSNotificationCenter defaultCenter]postNotificationName:kDataControllerNotificationKey object:error userInfo:nil];
}

@end
