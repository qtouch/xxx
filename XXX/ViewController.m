//
//  ViewController.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "ViewController.h"
#import "DataController.h"
#import "ThemeViewController.h"
#import "WHJThemeCollectionViewCell.h"



@interface ViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak,nonatomic) DataController* dc;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(DataController *)dc{
    if (!_dc) {
        _dc = [DataController sharedDataController];
    }
    return _dc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dc numTheme];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    WHJThemeCollectionViewCell* tcvc = [collectionView dequeueReusableCellWithReuseIdentifier:kThemeCellID forIndexPath:indexPath];
    tcvc.titleLable.text = [self.dc titleThemeOfIndex:indexPath.item];
    return tcvc;
}

#pragma mark - Event
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:kThemeSegue]) {
        [(ThemeViewController*)[segue destinationViewController] setThemeIndex:[[_collectionView indexPathForCell:sender] item]];
    }
}


@end
