//
//  RecodViewController.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString* kSimpleCell = @"simple";

@interface RecodViewController : UITableViewController

@property (nonatomic) NSUInteger themeIndex;
@property (nonatomic) NSUInteger recordIndex;

@end
