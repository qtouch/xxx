//
//  WHJDataObject.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, WHJDataObjectLoadStatus) {
    WHJDataObjectLoadStatusCantLoad         = 0,
    WHJDataObjectLoadStatusOnline           = 0x1,
    WHJDataObjectLoadStatusCached           = 0x1>>1,
    WHJDataObjectLoadStatusLoaded         = 0x1>>2,
};

@interface WHJDataObject : NSObject

@property (nonatomic) NSURL* url;
@property (nonatomic) NSString* fileName;
@property (nonatomic) NSString* bundleName;

//Status
@property (nonatomic,readonly) WHJDataObjectLoadStatus loadStatus;

-(instancetype)init;
-(instancetype)initWithURL:(NSURL*)url;
-(instancetype)initWithFileName:(NSString*)name bundle:(NSString*)bdName;
-(instancetype)initWithDictionary:(NSDictionary*)dictionary;


-(BOOL)load;
-(BOOL)unload;
-(BOOL)save;

@end