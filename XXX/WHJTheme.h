//
//  WHJTheme.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "WHJDataObject.h"

@class WHJRecord;

@interface WHJTheme : WHJDataObject

@property (nonatomic) NSString* title;
@property (nonatomic) BOOL isConcerned;

@property (nonatomic,readonly) NSArray* records;
-(void)addRecord:(WHJRecord*)record;


@end
