//
//  DataController.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* kDataControllerErrorKey = @"DataControllerError";
static NSString* kDataControllerNotificationKey = @"DataController";

static NSString* kRecordTitle = @"title";
static NSString* kRecordDate = @"date";
static NSString* kDocumentDiscuss = @"discuss";

@interface DataController : NSObject

+(instancetype)sharedDataController;

//VC1
-(NSUInteger)numTheme;
-(NSString*)titleThemeOfIndex:(NSUInteger)ti;

//VC2
-(NSUInteger)numRecordOfThemeIndex:(NSUInteger)ti;
-(NSString*)titleOfRecordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti;
-(NSDictionary*)dictionaryOfRecordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti;


//VC3
-(NSUInteger)numReplyOfRecordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti;
-(NSDictionary*)dictionaryOfDocumentOfIndex:(NSUInteger)di recordIndex:(NSUInteger)ri themeIndex:(NSUInteger)ti;

@end
