//
//  WHJTheme.m
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "WHJTheme.h"
#import "WHJRecord.h"

@implementation WHJTheme{
    NSMutableArray* _records;
}
@synthesize records = _records;

-(instancetype)init{
    if (self = [super init]) {
        _isConcerned = NO;
        _records = [NSMutableArray array];
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [self init]) {
        _title = dictionary[@"title"];
        _isConcerned = dictionary[@"isConcerned"];
        
        NSArray* a = dictionary[@"records"];
        for (NSDictionary* d in a) {
            WHJRecord* r = [[WHJRecord alloc]initWithDictionary:d];
            r.theme = self;
            [self addRecord:r];
        }
    }
    return self;
}

-(void)addRecord:(WHJRecord *)record{
    [_records addObject:record];
}

@end
