//
//  WHJDocument.h
//  XXX
//
//  Created by GatsWang on 15/10/13.
//  Copyright © 2015年 Qtouch. All rights reserved.
//

#import "WHJDataObject.h"
@class WHJRecord;


@interface WHJDocument : WHJDataObject

@property (nonatomic) NSString* date;
@property (nonatomic) NSString* discuss;

@property (nonatomic,weak) WHJRecord* record;

@end
